const {inventory} = require('./problem1')

function getCarYear(inventory){
    let carYearArr = []
    for(let i = 0 ; i < inventory.length; i++){
        carYearArr.push(inventory[i]["car_year"])
    }
  
    return carYearArr
}
module.exports.getCarYear =getCarYear;
module.exports.inventory = inventory;
