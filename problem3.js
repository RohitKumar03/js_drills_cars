const {inventory} = require('./problem1')

function getCarModels(inventory){
    let carModelArr = []
    for(let i = 0 ; i < inventory.length; i++){
        carModelArr.push(inventory[i]["car_model"].toUpperCase())
    }
  
    return carModelArr.sort()
}

module.exports.getCarModels =getCarModels;
module.exports.inventory = inventory
