const {findCar,inventory} = require('../problem1.js')
const carId = 33
let carData = findCar(inventory, carId)
console.log(`Car ${carData['id']} is a ${carData['car_year']} ${carData['car_model']}`)
