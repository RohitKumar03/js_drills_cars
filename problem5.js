const {getCarYear, inventory} = require('./problem4')

let carYearData = getCarYear(inventory)


function getOlderCar(carYearData, inputYear){ 
    let olderCarArr = []
    for(let i = 0 ; i < carYearData.length; i++){
          if(inputYear > carYearData[i]){ 
            olderCarArr.push(carYearData[i])
        }  
    }
    return olderCarArr
}
module.exports.getOlderCar =getOlderCar
module.exports.carYearData =carYearData