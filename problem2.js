const {inventory} = require('./problem1.js')

function getLastCar(inventory){
    let carData = inventory[inventory.length-1]
    return carData
}

module.exports.getLastCar = getLastCar
module.exports.inventory = inventory
